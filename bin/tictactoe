#!/usr/bin/env python

"""

Tic Tac Toe
Alex Launi

"""

import sys, logging, gtk, optparse
from os.path import join, dirname, exists, realpath, abspath


######################################################################
# Options and configuration
def parse_opts():
    usage = "usage: %prog [options]"

    oparse = optparse.OptionParser(usage=usage)

    oparse.add_option("-v", "--verbose", action="store_true",
                      help="Verbose messages", default=False)
    oparse.add_option("-d", "--debug", action="store_true",
                      help="Debug messages", default=False)
    
    opts, rest = oparse.parse_args()
    return opts
    
opts = parse_opts()

if opts.debug:
    logging.root.setLevel(logging.DEBUG)
elif opts.verbose:
    logging.root.setLevel(logging.INFO)


######################################################################
# Setup path

LAUNCH_DIR = abspath(sys.path[0])
logging.debug("Launched from %s", LAUNCH_DIR)
source_tree_tictactoe = join(LAUNCH_DIR, "..", "tictactoe")

# If we were invoked from a tictactoe source directory add that as the
# preferred module path ...
if exists(join(source_tree_tictactoe, "main.py")):
    logging.info("Running from source tree; adjusting path")
    sys.path.insert(0, realpath(dirname(source_tree_tictactoe)))
    try:
        from tictactoe.main import TicTacToeGame
    finally:
        del sys.path[0]
else:
    sys.path.insert(0, "/usr/lib/python2.6/site-packages/")
    logging.debug("Assuming path is correct")
    from tictactoe.main import TicTacToeGame


######################################################################
# Go...

TicTacToeGame()
gtk.main()
