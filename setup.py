#!/usr/bin/env python
#
# (C) 2009 Alex Launi

try:
    import DistUtilsExtra.auto
except:
    import sys
    print >> sys.stderr, 'To build tictactoe you need https://launchpad.net/python-distutils-extra'
    sys.exit(1)
    
MIN_DISTUTILSEXTRA_MAJOR = 2
MIN_DISTUTILSEXTRA_MINOR = 4
MIN_DISTUTILSEXTRA_MICRO = 0

dist_utils_version = map(int, DistUtilsExtra.auto.__version__.split("."))
while len(dist_utils_version) < 3:
    dist_utils_version.append(0)
    
major, minor, micro = dist_utils_version[0], dist_utils_version[1], dist_utils_version[2]
    
assert major >= MIN_DISTUTILSEXTRA_MAJOR
if major == MIN_DISTUTILSEXTRA_MAJOR:
    assert minor >= MIN_DISTUTILSEXTRA_MINOR
    if minor == MIN_DISTUTILSEXTRA_MINOR:
        assert micro >= MIN_DISTUTILSEXTRA_MICRO

DistUtilsExtra.auto.setup(
    name='tictactoe',
    version='0.3.2.1',
    description='A simple, fun tic tac toe game using telepathy tubes for two player action',
    url='http://launchpad.net/tictactoe',
    license='GNU General Public License (GPL)',
    author='Alex Launi',
    author_email='alex.launi@gmail.com',
    
    scripts = ['bin/tictactoe'],
)
