#!/usr/bin/python

# networking.py
# (C) 2009 Alex Launi - alex.launi@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.

import dbus

game_interface = "org.gnome.TicTacToe.game"

class Peer(dbus.service.Object):

    def __init__(self, bus, obj_path, board):
        """game is the UI in main.py, for which we should get a better name
        """
        dbus.service.Object.__init__(self, bus, obj_path)
        self._board = board

    @dbus.service.method(dbus_interface=game_interface, in_signature="", out_signature="")
    def new_game(self):
        self._board.new_game()

    @dbus.service.method(dbus_interface=game_interface, in_signature="iii", out_signature="")
    def make_move(self, x, y, player):
        x, y, player = int(x), int(y), int(player)
        self._board.make_move([x, y], player)
