#!/usr/bin/python

# tptube.py
# (C) 2009 Alex Launi - alex.launi@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.

import logging

import dbus
import gobject
from gettext import gettext as _
from telepathy.client.channel import Channel
from telepathy.interfaces import (
        CHANNEL_INTERFACE,
        CHANNEL_INTERFACE_TUBE,
        CHANNEL_TYPE_DBUS_TUBE,
        CONNECTION_INTERFACE_REQUESTS)
from telepathy.constants import (
        HANDLE_TYPE_CONTACT,
        SOCKET_ACCESS_CONTROL_CREDENTIALS,
        TUBE_CHANNEL_STATE_OPEN,
        TUBE_CHANNEL_STATE_REMOTE_PENDING)
import telepathy.errors

logger = logging.root

class TubeOffer(gobject.GObject):

    __gsignals__ = {
            "offer-succeeded": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                (object, object)),
            "offer-failed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                (str,)),
            }

    def __init__(self, conn, handle, tube_service):
        logger.debug('Create tube request')
        gobject.GObject.__init__(self)
        self._tube_state = None
        self._address = None
        self._chan = None
        self._closed_signal = None

        self._conn = conn
        self._handle = handle
        self._tube_service = tube_service

    def _tube_state_changed_cb(self, state):
        self._tube_state = state
        if state == TUBE_CHANNEL_STATE_OPEN:
            logger.debug('Tube state changed ->open')
            tube_conn = dbus.connection.Connection(self._address)
            self.emit("offer-succeeded", tube_conn, self._chan)
        elif state == TUBE_CHANNEL_STATE_REMOTE_PENDING:
            logger.debug('Tube state changed ->remote-pending')
        else:
            logger.debug('Tube state changed ->%s' % state)

    def _tube_closed_cb(self):
        logger.debug('tube closed')
        if self._tube_state == TUBE_CHANNEL_STATE_REMOTE_PENDING:
            # if tube is already open, it's not _offer_ failed (it's other
            # problems)
            self.emit("offer-failed", _("Connection closed by peer"))

    def cancel(self):
        if self._chan:
            logger.debug('Cancelling tube request')
            self._closed_signal.remove()
            self._chan[CHANNEL_INTERFACE].Close()

    def execute(self):
        request = {
            CHANNEL_INTERFACE + ".ChannelType": CHANNEL_TYPE_DBUS_TUBE,
            CHANNEL_INTERFACE + ".TargetHandleType": HANDLE_TYPE_CONTACT,
            CHANNEL_INTERFACE + ".TargetHandle": self._handle,
            CHANNEL_TYPE_DBUS_TUBE + ".ServiceName": self._tube_service}
        try:
            path, props = self._conn[CONNECTION_INTERFACE_REQUESTS].CreateChannel(request)
        except dbus.exceptions.DBusException, excep:
            error_busname = telepathy.errors.NotImplemented().get_dbus_name()
            if excep.get_dbus_name() == error_busname:
                msg = ('REQUESTS interface not implemented on %s' %
                       self._conn.service_name)
                self.emit('offer-failed', msg)
                return
            else:
                raise

        self._chan = Channel(self._conn.service_name, path)
        self._chan[CHANNEL_INTERFACE_TUBE].connect_to_signal(
            'TubeChannelStateChanged', self._tube_state_changed_cb)
        self._closed_signal = self._chan[CHANNEL_INTERFACE].connect_to_signal(
            'Closed', self._tube_closed_cb)

        logger.debug('offering tube')
        try:
            data = ""
            self._address = self._chan[CHANNEL_TYPE_DBUS_TUBE].Offer(
                data,
                SOCKET_ACCESS_CONTROL_CREDENTIALS)
        except dbus.exceptions.DBusException, excep:
            error_busname = telepathy.errors.NotAvailable().get_dbus_name()
            if excep.get_dbus_name() == error_busname:
                msg = str(excep)
                self.emit("offer-failed", msg)
                return
            else:
                raise
