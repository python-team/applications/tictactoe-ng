#!/usr/bin/python

# contactselector.py
# (C) 2009 Alex Launi - alex.launi@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.

import os
import logging

import gtk
import dbus.mainloop.glib

import telepathy
from xdg import BaseDirectory

from telepathy.interfaces import (
    CHANNEL,
    CHANNEL_INTERFACE_GROUP,
    CHANNEL_TYPE_CONTACT_LIST,
    CONNECTION,
    CONNECTION_INTERFACE_ALIASING,
    CONNECTION_INTERFACE_AVATARS,
    CONNECTION_INTERFACE_CONTACTS,
    CONNECTION_INTERFACE_REQUESTS,
    CONNECTION_INTERFACE_SIMPLE_PRESENCE)

from telepathy.constants import (
    CONNECTION_PRESENCE_TYPE_AVAILABLE,
    CONNECTION_PRESENCE_TYPE_AWAY,
    CONNECTION_PRESENCE_TYPE_BUSY,
    CONNECTION_PRESENCE_TYPE_EXTENDED_AWAY,
    HANDLE_TYPE_LIST)

dbus.mainloop.glib.DBusGMainLoop(set_as_default = True)
theme = gtk.icon_theme_get_default()
logger = logging.root

cache_dir = os.path.join(BaseDirectory.xdg_cache_home, 'tictactoe')
avatar_cache_dir = os.path.join(cache_dir, 'avatars')

if not os.path.exists(cache_dir):
    os.mkdir(cache_dir)

if not os.path.exists(avatar_cache_dir):
    os.mkdir(avatar_cache_dir)

def dbus_int_list_to_string(dbus_list):
    """Convert a dbus-list of integer to normal python one

    dbus_list can also be a normal python list; can't hurt"""
    return [int(x) for x in dbus_list]

class _Contact:
    """Help class for the contact to be stored in ContactListStore"""

    def __init__(self, contact_id, alias, presence, avatar):
        self.contact_id = contact_id
        self.alias = alias
        self.presence = presence
        self.avatar = avatar

class ContactList:
    """the list of contacts from *one* connection"""

    def __init__(self, list_store, conn):
        self._list_store = list_store
        self._conn = conn

        # dict of {handle=>Contact}
        # If ListStore supports searching, this _contact_list is not needed.
        self._contact_list = {}

        self._conn.call_when_ready(self._connection_ready_cb)

    def _connection_ready_cb(self, conn):
        logger.debug('Connection is ready: %s' % conn.service_name)
        # TODO react to connection status change

        if CONNECTION_INTERFACE_SIMPLE_PRESENCE not in conn:
            logger.warning('SIMPLE_PRESENCE interface not available on %s' % conn.service_name)
            return
        if CONNECTION_INTERFACE_REQUESTS not in conn:
            logger.warning('REQUESTS interface not available on %s' % conn.service_name)
            return

        if CONNECTION_INTERFACE_AVATARS not in conn:
            logger.warning('AVATARS interface not available on %s' % conn.service_name)
        else:
           self._conn[CONNECTION_INTERFACE_AVATARS].connect_to_signal(
               'AvatarRetrieved', self._avatar_retrieved_cb, byte_arrays=True)

        conn[CONNECTION_INTERFACE_SIMPLE_PRESENCE].connect_to_signal(
            'PresencesChanged', self._contact_presence_changed_cb)
        self._ensure_channel()

    def _ensure_channel(self):
        """Request the channel we needed to fetch contact-list"""
        # TODO is it ok to include only these two groups?
        groups = ["subscribe", "publish"]
        for group in groups:
            requests = {
                CHANNEL + ".ChannelType": CHANNEL_TYPE_CONTACT_LIST,
                CHANNEL + ".TargetHandleType": HANDLE_TYPE_LIST,
                CHANNEL + ".TargetID": group}
            self._conn[CONNECTION_INTERFACE_REQUESTS].EnsureChannel(
                requests,
                reply_handler = self._ensure_channel_cb,
                error_handler = self._error_cb)

    def _ensure_channel_cb(self, is_yours, channel, properties):
        """Got channel"""
        logger.debug('new channel ensured: channel=%s; it-is-yours=%s' % (channel, is_yours))
        channel = telepathy.client.Channel(
            service_name = self._conn.service_name,
            object_path = channel)
        DBUS_PROPERTIES = 'org.freedesktop.DBus.Properties'
        channel[DBUS_PROPERTIES].Get(
            CHANNEL_INTERFACE_GROUP,
            'Members',
            reply_handler = self._request_contact_info,
            error_handler = self._error_cb)

    def _request_contact_info(self, handles):
        """Request contact-info for a list of contact-handles"""
        interfaces = [CONNECTION,
                      CONNECTION_INTERFACE_ALIASING,
                      CONNECTION_INTERFACE_AVATARS,
                      CONNECTION_INTERFACE_SIMPLE_PRESENCE]
        self._conn[CONNECTION_INTERFACE_CONTACTS].GetContactAttributes(
            handles,
            interfaces,
            False,
            reply_handler = self._get_contact_attributes_cb,
            error_handler = self._error_cb)

        self._conn[CONNECTION_INTERFACE_AVATARS].RequestAvatars(handles)

    def _avatar_retrieved_cb(self, contact, token, avatar, type):
        path = os.path.join(avatar_cache_dir, token)
        if os.path.exists(path):
            return
        file = open(path, 'wb')
        file.write(avatar)
        file.close()

    def _get_contact_attributes_cb(self, attributes):
        for handle, member in attributes.iteritems():
            contact_info = self._parse_member_attributes(member)
            contact = _Contact(*contact_info)
            if handle not in self._contact_list:
                self._add_contact(handle, contact)

    def _parse_member_attributes(self, member):
        """Parse contact information out from dbus.Struct member"""
        contact_id, alias, presence, avatar = None, None, None, None
        # TODO won't these be in a certain order?

        for key, value in member.iteritems():
            if key == CONNECTION + '/contact-id':
                contact_id = value
            elif key == CONNECTION_INTERFACE_ALIASING + '/alias':
                alias = value
            elif key == CONNECTION_INTERFACE_AVATARS + '/token':
                avatar = value
            elif key == CONNECTION_INTERFACE_SIMPLE_PRESENCE + '/presence':
                presence = value

        # TODO Can they be None?
        assert contact_id is not None
        assert presence is not None
        assert alias is not None
        return (contact_id, alias, presence, avatar)

    def _add_contact(self, handle, contact):
        self._contact_list[handle] = contact
        self._list_store.add_contact(self._conn, handle, contact)

    def _contact_presence_changed_cb(self, presences):
        for handle, presence in presences.iteritems():
            if handle in self._contact_list:
                self._update_contact_presence(handle, presence)
            else:
                # request its info and consequently add it to the list
                self._request_contact_info([handle])

    def _update_contact_presence(self, handle, presence):
        logger.debug('update presence of %s' % self._contact_list[handle].alias)
        self._contact_list[handle].presence = presence
        self._list_store.update_contact_presence(self._conn, handle, presence)

    def _error_cb(self, *args):
        logger.error('Error happens: %s' % args)

class ContactListStore(gtk.ListStore):
    """
    ListStore that contains contact list from (all connections of) telepathy
    """

    # indices of columns that are of interest to display
    COL_CONTACT = 2

    def __init__(self):
        # row format: <Connection>, <handle>, <Contact>, <alias>, <pixbuf>
        gtk.ListStore.__init__(self, object, int, object, str, gtk.gdk.Pixbuf)

        # dict of {connection->ContactList}
        self._contact_lists = {}

        connections = self._get_telepathy_connections()
        # TODO: what if new connection is available?
        self._watch_contacts(connections)

    def _get_telepathy_connections(self):
        """Return telepathy connections on dbus

        Use a separate method for the sake of easy test"""
        return telepathy.client.Connection.get_connections()

    def _watch_contacts(self, connections):
        if not connections:
            logger.warning('No telepathy connections available')
            return

        logger.debug('Got %s telepathy connections' % len(connections))

        for conn in connections:
            self._contact_lists[conn] = ContactList(self, conn)

    def add_contact(self, conn, handle, contact):
        """
        Add a row to ListStore.
        conn and handle are used to uniquely refer to a certain row/contact
        """
        if contact.avatar is None:
            avatar = self._load_default_avatar()
        else:
            avatar = self._load_contact_avatar(contact)
        self.append((conn, handle, contact, contact.alias, avatar))

    def update_contact_presence(self, conn, handle, presence):
        # TODO: bruteforce
        for row in self:
            if (row[0], row[1]) == (conn, handle):
                self[row.iter][self.COL_CONTACT].presence = presence
                self.row_changed(row.path, row.iter)

    def _load_contact_avatar(self, contact):
        path = os.path.join(avatar_cache_dir, contact.avatar)
        if os.path.exists(path):
            try:
                avatar = gtk.gdk.pixbuf_new_from_file(path)
                avatar = avatar.scale_simple(48, 48, gtk.gdk.INTERP_BILINEAR)
                return avatar
            except:
                pass

        return self._load_default_avatar()

    def _load_default_avatar(self):
        return theme.load_icon("stock_people", 48, 0)

class ContactSelector(gtk.IconView):
    """An Icon view for selecting contacts"""

    COL_CONTACT = 2
    COL_NAME = 3
    COL_PIXBUF = 4

    def __init__(self):
        list_store = ContactListStore()
        list_store.set_sort_func(1, self._sort_contacts)
        list_store.set_sort_column_id(1, gtk.SORT_ASCENDING)
        online_model_filter = list_store.filter_new()
        online_model_filter.set_visible_func(self._filter_online_contacts)

        super(ContactSelector, self).__init__(online_model_filter)

        self.set_selection_mode(gtk.SELECTION_SINGLE)
        self.set_text_column(self.COL_NAME)
        self.set_pixbuf_column(self.COL_PIXBUF)
        self.set_margin(2)
        self.set_column_spacing(2)
        self.set_row_spacing(2)
        self.set_columns(4)

    def _sort_contacts(self, model, iter1, iter2):
        contact1 = self._get_contact(model, iter1)
        contact2 = self._get_contact(model, iter2)
        if contact1 is not None and contact2 is not None:
            return contact1.alias.lower() > contact2.alias.lower()
        return 0

    def _filter_online_contacts(self, model, treeiter):
        online_statues = [
            CONNECTION_PRESENCE_TYPE_AVAILABLE,
            CONNECTION_PRESENCE_TYPE_AWAY,
            CONNECTION_PRESENCE_TYPE_BUSY,
            CONNECTION_PRESENCE_TYPE_EXTENDED_AWAY]
        contact = self._get_contact(model, treeiter)
        if contact:
            return contact.presence[0] in online_statues
        else:
            return False

    def _get_contact(self, model, treeiter):
        contact_column = ContactListStore.COL_CONTACT
        return model[treeiter][contact_column] # TODO: why contact can be None?

